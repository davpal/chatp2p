package chatp2p;

public interface Observer {
  void update(Observable o, Object arg);
}