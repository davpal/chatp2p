package chatp2p;

public interface Observable {
  void notifyObservers(Object o);
  void addObserver(Observer o);
}