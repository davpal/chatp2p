package chatp2p;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.net.*;

public class Peer implements Runnable, Observable {

    private String nick;
    private InetAddress address;

    PeerSocket socket;
    PeerSocket sendSocket;

    private boolean connected;

    List<Peer> connectedPeers = new ArrayList<Peer>();
    List<Observer> observers = new ArrayList<Observer>();

    Peer(String n, InetAddress a, boolean data) {
        nick = n;
        address = a;
    }

    Peer(String n, InetAddress a) {
        try {
            address = a;
            socket = new PeerSocket(Chat.PORT, a);
            sendSocket = new PeerSocket(Chat.SEND_PORT, a);
        } catch (Exception e) {
            e.printStackTrace();
        }
        nick = n;
        connected = true;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String n) {
        nick = n;
    }

    public InetAddress getAddress() {
        return address;
    }

    public void setAddress(InetAddress a) {
        address = a;
    }

    public void send(Message m) {
        sendSocket.send(m);
    }

    public void send(String text) {
        List<String> receivers = new ArrayList<String>();
        int index, current = 0, separatorIndex = 0;

        /*
         * Parsowanie wiadomości w postaci @user1 @user2 ...
         * Przesyłanie jej do wyspecyfikowanych odbiorców
         */
        while ((index = text.indexOf('@', current)) != -1
                || (index = text.indexOf('#', current)) != -1) {
            separatorIndex = text.indexOf(' ', index + 1);
            receivers.add(text.substring(index + 1, separatorIndex));
            current += separatorIndex;
        }
        text.substring(0, separatorIndex);

        if (!receivers.isEmpty()) {
            for (int i = 0; i < connectedPeers.size(); ++i) {
                for (int j = 0; j < receivers.size(); ++j) {
                    if (connectedPeers.get(i).getNick().equals(receivers.get(j))) {
                        connectedPeers.get(i);
                        Message message
                                = new Message(Message.DATA, this, connectedPeers.get(i), text);
                        sendSocket.send(message);
                    }
                }
            }
        } else {
            Message message = new Message(this, text);
            sendSocket.send(message);
        }
        notifyObservers("<" + getNick() + "> " + text);
    }

    public void close() {
        connected = false;
        notifyObservers("/logout");
        Message quit = new Message(Message.QUIT, this);
        sendSocket.send(quit);
    }

    public boolean isConnected() {
        return connected;
    }

    public boolean isBusy() {
        Iterator<Peer> iterator = connectedPeers.iterator();
        while (iterator.hasNext()) {
            Peer peer = (Peer) iterator.next();
            if (peer.getNick().equals(nick)) {
                return true;
            }
        }
        return false;
    }

    public void connect() {
        if (isBusy()) {
            connectedPeers.clear();
            connected = false;
            notifyObservers("/busy");
            Message quit = new Message(Message.QUIT, this, this);
            sendSocket.send(quit);
        } else {
            Message login = new Message(Message.LOGIN, this);
            sendSocket.send(login);
            Iterator<Peer> iterator = connectedPeers.iterator();
            while (iterator.hasNext()) {
                Peer peer = (Peer) iterator.next();
                notifyObservers("/join " + peer.getNick());
            }
            notifyObservers("/connected");
        }
    }

    public void run() {
        Message connect = new Message(Message.CONNECT, this);
        sendSocket.send(connect);
        while (connected) {
            Message reply = socket.receive();
            Peer sender = reply.getSender();

            /* Przechwyc QUIT z lokalnego hosta by
             * nie zablokować wątku na metodzie receive().
             * Pomin reszte pakietów wysłanych "do siebie"
             */
            try {
                if (sender.getAddress().equals(address)) {
                    switch (reply.getCode()) {
                        case Message.QUIT:
                            socket.close();
                            sendSocket.close();
                            break;
                    }
                    continue;
                }
            } catch (Exception e) {

            }

            switch (reply.getCode()) {
                case Message.PEER:
                    connectedPeers.add(sender);
                    break;
                case Message.CONNECT:
                    Message me = new Message(Message.PEER, this, sender);
                    sendSocket.send(me);
                    break;
                case Message.LOGIN:
                    connectedPeers.add(sender);
                    notifyObservers("/login " + sender.getNick());
                    break;
                case Message.LOGOUT:
                    connectedPeers.remove(sender);
                    notifyObservers("/logout " + sender.getNick());
                case Message.QUIT:
                    connectedPeers.remove(sender);
                    notifyObservers("/quit " + sender.getNick());
                    break;
                default:
                    notifyObservers("<" + sender.getNick() + "> " + reply.toString());
            }
        }
    }

    public void addObserver(Observer o) {
        observers.add(o);
    }

    public void notifyObservers(Object o) {
        Iterator<Observer> it = observers.iterator();
        while (it.hasNext()) {
            Observer observer = it.next();
            observer.update(this, o);
        }
    }
}
