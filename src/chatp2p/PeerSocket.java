package chatp2p;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class PeerSocket {

    DatagramSocket socket;

    PeerSocket(int port, InetAddress address) throws Exception {
        socket = new DatagramSocket(port, address);
    }

    public void send(Message m) {
        try {
            socket.send(m.toPacket());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Message receive() {
        byte[] data = new byte[Message.MAX_SIZE];
        DatagramPacket packet = new DatagramPacket(data, data.length);
        try {
            socket.receive(packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Message.parsePacket(packet);
    }

    public void close() {
        socket.close();
    }
}
