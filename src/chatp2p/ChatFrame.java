package chatp2p;

import javax.swing.*;

import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.*;
import java.net.*;

public class ChatFrame extends JFrame implements Observer {
  private JTextArea messages;
  private JTextArea send;
  private JList<String> users;
  private DefaultListModel<String> model = new DefaultListModel<String>();
  private JTextField nick;
  private JButton connect;
  JButton sendButton;
  JComboBox<String> interfaceBox;
  
  List<InetAddress> networkAddresses = new ArrayList<InetAddress>();
  
  Peer peer;
  boolean connected;
  
  public ChatFrame(){
	  setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      setSize(700, 520);
	  initComponents();
	  setVisible(true);
	  
	  /* Przechwycenie zdarzenia zamknięcia ramki */
	  this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        if(connected){
		        	disconnect();
		        	peer.close();
		        }
		        System.exit(0);
		    }
		});
  }

  void initComponents(){
    JPanel main = new JPanel(new BorderLayout());

	  messages = new JTextArea();
	  messages.setEditable(false);
	  JScrollPane messageScroller = new JScrollPane(messages);
	
	  /*
	   * Obszar z wiadomościami ustawiony w tryb "autoscroll"
	   */
	  messageScroller.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {  
      public void adjustmentValueChanged(AdjustmentEvent e) {  
        e.getAdjustable().setValue(e.getAdjustable().getMaximum());  
      }}); 
	
	  users = new JList<String>(model);
	  users.setLayoutOrientation(JList.VERTICAL);
	  JScrollPane listScroller = new JScrollPane(users);
	  listScroller.setPreferredSize(new Dimension(200, 450));
	  GUIListener guil = new GUIListener();
	
	  send = new JTextArea();
	  send.setEnabled(false);
	  send.addKeyListener(guil);
	  JScrollPane sendScroller = new JScrollPane(send);
	  sendScroller.setPreferredSize(new Dimension(500, 100));
	
	  // Buduj górny panel
	  JPanel top = new JPanel(new FlowLayout(FlowLayout.CENTER));
	  top.add(new JLabel("Adres sieciowy: "));
	  interfaceBox = new JComboBox<String>();
	  
	  Enumeration<NetworkInterface> nics = null;
	  try {
		  nics = NetworkInterface.getNetworkInterfaces();
	  } catch (SocketException e) {}
		
	  while(nics.hasMoreElements()){
		  NetworkInterface nic = nics.nextElement();
		  try {
			  if(nic.isLoopback()) continue;
		  } catch(Exception e){
			  
		  }
		  Enumeration<InetAddress> addresses = nic.getInetAddresses();
		  if(!addresses.hasMoreElements()) continue;
		  InetAddress address = addresses.nextElement();
		  if(!(address instanceof Inet4Address)) continue;
		  interfaceBox.addItem(address.toString());
		  networkAddresses.add(address);
	  } 
	  
	  top.add(interfaceBox);
	  
	  top.add(new JLabel("Twój nick: "));
	  nick = new JTextField(10);
	  top.add(nick);
	  connect = new JButton("Połącz");
	  connect.addActionListener(guil);
	  top.add(connect);
	
	  // Buduj panel użytkowników
	  JPanel right = new JPanel();
	  right.setLayout(new BoxLayout(right, BoxLayout.PAGE_AXIS));
	  right.add(new JLabel("Zalogowani: "));
	  right.add(Box.createVerticalGlue()); 
	  right.add(listScroller);
	
	  // Buduj panel wiadomości
	  JPanel msgPanel = new JPanel();
	  msgPanel.setLayout(new BoxLayout(msgPanel, BoxLayout.LINE_AXIS));
	  msgPanel.add(new JLabel("Napisz wiadomość: "));
	  sendButton = new JButton("Wyślij");
	  sendButton.setEnabled(false);
	  sendButton.addActionListener(new ActionListener(){
	    public void actionPerformed(ActionEvent e){
	      sendMessage();
	    }
	  });
	  msgPanel.add(sendButton);
	
	  // Buduj panel środkowy
	  JPanel center = new JPanel();
	  center.setLayout(new BoxLayout(center, BoxLayout.PAGE_AXIS));
	  center.add(new JLabel("Wiadomości: "));
	  messageScroller.setPreferredSize(new Dimension(500, 300));
	  center.add(Box.createVerticalGlue()); 
	  center.add(messageScroller);
	  center.add(Box.createVerticalGlue()); 
	  center.add(msgPanel);
	  center.add(Box.createVerticalGlue()); 
	  center.add(sendScroller);
	
	  main.add(top, BorderLayout.PAGE_START);
	  main.add(center, BorderLayout.CENTER);
	  main.add(right, BorderLayout.LINE_END);
	
	  add(main);	
  }

  private class GUIListener extends KeyAdapter implements ActionListener {
 
    public void actionPerformed(ActionEvent e){
	    if(nick.getText().equals("")) return;
	    if(!connected){
	      connect();
	    } else { 
	      disconnect();
		  peer.close();
	    }
	  }
	
	  /*
	   * Metoda wysyła wiadomość w wypadku użycia klawisza ENTER.
	   *
	   */
	  public void keyReleased(KeyEvent e){
	    if(e.getKeyCode() == KeyEvent.VK_ENTER){ 
	      sendMessage();
	    }
	  }
  }
  
  private void connect(){
	  try {
         peer = new Peer(nick.getText(), networkAddresses.get(interfaceBox.getSelectedIndex()));
         peer.addObserver(this);
         
         Thread peerThread = new Thread(peer);
         peerThread.start();
         
	  } catch(Exception e){
		 e.printStackTrace();
	  }
	  
	  connected = true;
	  connect.setText("Rozłącz");
	  send.setEnabled(true);
	  sendButton.setEnabled(true);
	  nick.setEnabled(false);
	  
	  /*
	   * Niezbędne opóźnienie potrzebne do zebrania
	   * danych wszystkich zalogowanych klientów.
	   */
	  try {
	    Thread.sleep(2000);
	    peer.connect();
	  } catch(InterruptedException e){
		  
	  }
  }
  
  private void disconnect(){
	  connected = false;
      connect.setText("Połącz");
	  send.setEnabled(false);
	  nick.setEnabled(true);
	  sendButton.setEnabled(false); 
	  model.clear();
  }
  
  private void sendMessage(){
	  String text = send.getText();
	  text = text.replace('\n', '\t');
      if(!text.equals("")) peer.send(text);
	  send.setText("");
  }
  
  /*
   * Każda zmiana w aplikacji rejestrowana jest poprzez tę metodę
   */
  public void update(Observable o, Object arg){
      String s = arg.toString();
	  String text = s;
	  String[] parts = s.split(" "); 
      if(s.startsWith("/login")){ 
        model.addElement(parts[1]); 
	    text = "Użytkownik " + parts[1] + " wszedł na czat.";
	  } else if(s.startsWith("/join")){ 
	    model.addElement(parts[1]);
	    return;
	  } else if(s.startsWith("/quit")){ 
	    text = "Użytkownik " + parts[1] + " wyszedł z czatu.";
	    model.removeElement(parts[1]);
	  } else if(s.startsWith("/logout")){ 
		if(parts.length < 2){
	      text = "Rozłączyłeś się.";
	      disconnect();
		} else {
			model.removeElement(parts[1]);
			return;
		}
	  } else if(s.startsWith("/busy")){
		 text = "Podany nick jest zajęty!";
		 disconnect();
	  } else if(s.startsWith("/connected")){
		 text = "Połączyłeś się.";
	  }
  
    Calendar calendar = Calendar.getInstance();
  
	  String date = "(" + 
	    calendar.get(Calendar.HOUR_OF_DAY) + ":" +
	    calendar.get(Calendar.MINUTE) + ":" +
	    calendar.get(Calendar.SECOND) + ") ";
  
     messages.append(date + text + "\n");
  }  
}