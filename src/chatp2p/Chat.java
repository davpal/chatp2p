package chatp2p;

import java.net.*;

import javax.swing.*;

public class Chat {

    public static final int PORT = 50000;
    public static final int SEND_PORT = 50001;
    public static InetAddress BROADCAST;

    public static void main(String[] args) {
        try {
            /*
             * Sprawdzamy czy klient jest już podłączony na tym komputerze
             */
            DatagramSocket socket = new DatagramSocket(Chat.PORT);
            BROADCAST = InetAddress.getByName("255.255.255.255");
            socket.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Klient jest już uruchomiony na tym komputerze.", "Informacja",
                    JOptionPane.INFORMATION_MESSAGE);
            System.exit(1);
        }
        new ChatFrame();
    }
}
