package chatp2p;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.nio.ByteBuffer;

public class Message {

    public static final byte DATA = 0;
    public static final byte CONNECT = 1;
    public static final byte LOGIN = 2;
    public static final byte LOGOUT = 3;
    public static final byte QUIT = 4;
    public static final byte PEER = 5;
    public static final int MAX_SIZE = 1024;

    private String text;
    private byte code;

    Peer sender, receiver;

    Message(Peer s, String t) {
        code = 0;
        text = t;
        sender = s;
        receiver = new Peer("BROADCAST", Chat.BROADCAST, true);
    }

    Message(byte c, Peer s, Peer r, String t) {
        code = c;
        sender = s;
        receiver = r;
        text = t;
    }

    Message(byte c, Peer s, Peer r) {
        sender = s;
        receiver = r;
        text = "";
        code = c;
    }

    Message(byte c, Peer s) {
        sender = s;
        receiver = new Peer("BROADCAST", Chat.BROADCAST, true);
        code = c;
        text = "";
    }

    public String toString() {
        return text;
    }

    public byte getCode() {
        return code;
    }

    public Peer getSender() {
        return sender;
    }

    public Peer getReceiver() {
        return receiver;
    }

    /* 
     * Budowanie obiektu Message z pakietu.
     * Struktura pakietu w README
     */
    public static Message parsePacket(DatagramPacket packet) {
        ByteBuffer buffer = ByteBuffer.wrap(packet.getData());
        byte code = buffer.get();
        byte[] address = new byte[4];
        buffer.get(address);
        byte[] nick = new byte[buffer.get()];

        Peer sender = null, receiver = null;
        try {
            buffer.get(nick);
            sender = new Peer(new String(nick), InetAddress.getByAddress(address), true);
            buffer.get(address);
            nick = new byte[buffer.get()];
            buffer.get(nick);
            receiver = new Peer(new String(nick), InetAddress.getByAddress(address), true);
        } catch (Exception e) {

        }

        int messageSize = buffer.getInt();
        byte[] message = new byte[messageSize];
        buffer.get(message, 0, messageSize);

        return new Message(code, sender, receiver, new String(message));
    }

    /* 
     * Budowanie pakietu z obiektu Message.
     * Struktura pakietu w README
     */
    public DatagramPacket toPacket() {
        ByteBuffer buffer = ByteBuffer.allocate(Message.MAX_SIZE);
        buffer.put(code);
        buffer.put(sender.getAddress().getAddress());
        byte[] nick = sender.getNick().getBytes();
        buffer.put((byte) nick.length);
        buffer.put(nick);
        buffer.put(receiver.getAddress().getAddress());
        nick = receiver.getNick().getBytes();
        buffer.put((byte) nick.length);
        buffer.put(nick);
        buffer.putInt(text.length());
        buffer.put(text.getBytes());

        DatagramPacket packet
                = new DatagramPacket(buffer.array(), buffer.capacity(),
                        receiver.getAddress(), Chat.PORT);

        return packet;
    }
}
